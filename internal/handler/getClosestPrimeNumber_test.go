package handler

import (
	"fmt"
	"testing"
)

func assertEqual(t *testing.T, a interface{}, b interface{}, message string) {
	if a == b {
		return
	}
	if len(message) == 0 {
		message = fmt.Sprintf("The test case is failed. %v != %v", a, b)
	}
	t.Fatal(message)
}

//
// test and data for TestGetClosestPrimeNumber
//
func TestGetClosestPrimeNumber(t *testing.T) {
	assertEqual(t, getHighestPrimeNumber(123), int64(113), "")
	assertEqual(t, getHighestPrimeNumber(11), int64(7), "")
	assertEqual(t, getHighestPrimeNumber(1), int64(-1), "")
}
