package handler

import (
	"encoding/json"
	"math/big"
	"strconv"

	"github.com/go-martini/martini"
)

// Response ...
type Response struct {
	ErrorMsg string
	Result   int64
}

func getHighestPrimeNumber(x int64) int64 {
	highestPrimeNumber := int64(-1)
	for i := x - 1; i >= 2; i-- {
		if big.NewInt(i).ProbablyPrime(0) {
			highestPrimeNumber = i
			break
		}
	}
	return highestPrimeNumber
}

// GetClosestPrimNumberHandler ...
func GetClosestPrimNumberHandler(params martini.Params) string {
	number, err := strconv.Atoi(params["number"])

	if err != nil || number < 0 {
		responseData, _ := json.Marshal(Response{ErrorMsg: "Number too big or invalid", Result: int64(-1)})
		return string(responseData)
	}

	bigNumber := int64(number)
	highestPrimeNumber := getHighestPrimeNumber(bigNumber)
	if highestPrimeNumber < 1 {
		responseData, _ := json.Marshal(Response{ErrorMsg: "Can't find prime number", Result: int64(highestPrimeNumber)})
		return string(responseData)
	}
	responseData, _ := json.Marshal(Response{ErrorMsg: "", Result: int64(highestPrimeNumber)})
	return string(responseData)
}
