package main

import (
	"stably/internal/handler"

	"github.com/go-martini/martini"
)

func main() {
	server := martini.Classic()
	server.Get("/is_prime/:number", handler.GetClosestPrimNumberHandler)
	server.Use(martini.Static("index.html"))
	server.Run()
}
